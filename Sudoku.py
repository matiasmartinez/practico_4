#sudoku.py

#tablero inicial (0 = espacio vacio) 

tablero = [
    [5, 3, 0, 0, 7, 0, 0, 0, 0],
    [6, 0, 0, 1, 9, 5, 0, 0, 0],
    [0, 9, 8, 0, 0, 0, 0, 6, 0],
    [8, 0, 0, 0, 6, 0, 0, 0, 3],
    [4, 0, 0, 8, 0, 3, 0, 0, 1],
    [7, 0, 0, 0, 2, 0, 0, 0, 6],
    [0, 6, 0, 0, 0, 0, 2, 8, 0],
    [0, 0, 0, 4, 1, 9, 0, 0, 5],
    [0, 0, 0, 0, 8, 0, 0, 7, 9]
]
'''
solución = [
    [5, 3, 4, 6, 7, 8, 9, 1, 2],
    [6, 7, 2, 1, 9, 5, 3, 4, 8],
    [1, 9, 8, 3, 4, 2, 5, 6, 7],
    [8, 5, 9, 7, 6, 1, 4, 2, 3],
    [4, 2, 6, 8, 5, 3, 7, 9, 1],
    [7, 1, 3, 9, 2, 4, 8, 5, 6],
    [9, 6, 1, 5, 3, 7, 2, 8, 4],
    [2, 8, 7, 4, 1, 9, 6, 3, 5],
    [3, 4, 5, 2, 8, 6, 1, 7, 9]
]
'''
#mostrar tablero de sudoku
def mostrar_tablero(tablero):
    for i in range(len(tablero)):
        if i % 3 == 0 and i != 0:
            print("- - - - - - - - - - - -")
        for j in range(len(tablero[0])):
            if j % 3 == 0 and j != 0:
                print(" | ", end="")
            if j == 8:
                print(tablero[i][j])
            else:
                print(str(tablero[i][j]) + " ", end="")

#verificar si el número está en la fila
def ver_fila(tablero, fila, num):
    for i in range(len(tablero[0])):
        if tablero[fila][i] == num:
            return False
    return True

#verificar si el número está en la columna
def ver_col(tablero, col, num):
    for i in range(len(tablero)):
        if tablero[i][col] == num:
            return False
    return True

#verificar si el número está en el caja
def ver_caja(tablero, fila, col, num):
    for i in range(3):
        for j in range(3):
            if tablero[i + fila][j + col] == num:
                return False
    return True

#verificar si el número es válido en la fila, columna y caja
def ver_num(tablero, fila, col, num):
    return ver_fila(tablero, fila, num) and ver_col(tablero, col, num) and ver_caja(tablero, fila - fila % 3, col - col % 3, num)

#resolver sudoku
def resolver(tablero):
    for i in range(len(tablero)):
        for j in range(len(tablero[0])):
            if tablero[i][j] == 0:
                for num in range(1, 10):               #asignar el número a la matriz
                    if ver_num(tablero, i, j, num):
                        tablero[i][j] = num
                        if resolver(tablero):          #llamar a la función recursivamente para verificar el siguiente número en la fila, columna y caja
                            return True
                tablero[i][j] = 0                      #si el número no es válido, asignarlo a 0 y llama a la función recursivamente para verificar el siguiente número en la fila, columna y caja
                return False
    return True                                        #terminar la función cuando el tablero esté lleno y el tablero es válido

#función main
def main():
    print("Tablero sudoku inicial:")
    print("=======================") 
    mostrar_tablero(tablero)               #mostrar el tablero inicial
    print("=======================")
    print("\n")
    resolver(tablero)                      #resolver el tablero
    print("Tablero sudoku solucionado:")
    print("=======================")
    mostrar_tablero(tablero)               #mostrar el tablero solucionado
    print("=======================")

#main
main()